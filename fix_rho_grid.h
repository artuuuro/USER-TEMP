/**
 * temp/grid
 * 
 * LAMMPS fix to calculate system temperature on a grid and store it in
 * a file. Also some averaging is done if needed.
 * 
 * Created by Artur Tamm <code@arturtamm.ee>
 * License GPL v3
 **/

/*
 * WORK IN PROGRESS
 */

#ifdef FIX_CLASS

FixStyle(rho/grid, FixRhoGrid);

#else

#ifndef LMP_FIX_RHO_GRID_H
#define LMP_FIX_RHO_GRID_H

#include "fix.h"

#include <vector>

namespace LAMMPS_NS {

class FixRhoGrid : public Fix {
  public:
    constexpr static int fn_size = 256;
    FixRhoGrid(class LAMMPS *, int, char **);
    
    int setmask() {
      int mask = 0;
      mask |= FixConst::END_OF_STEP;
      
      return mask;
    }
    
    void init();
    void end_of_step();
    
    double compute_scalar();
    double compute_vector(int index);
    
    double memory_usage();
    
  private:
    int my_id;
    int nr_ps;
  
    int n_x;
    int n_y;
    int n_z;
    double x_lo, x_hi;
    double y_lo, y_hi;
    double z_lo, z_hi;
    double dx, dy, dz;
    
    // x goes fastest
    std::vector<int> n_average;
    std::vector<double> m_average;
    std::vector<double> v_x_average;
    std::vector<double> v_y_average;
    std::vector<double> v_z_average;
    std::vector<double> temperature_x;
    std::vector<double> temperature_y;
    std::vector<double> temperature_z;
    
    double sum_temperature_x;
    double sum_temperature_y;
    double sum_temperature_z;
    double sum_temperature;
    
    int n_every; // how often to sample
    int n_freq; // how often to average
    int n_out; // how often to save output
    int n_step; // step counter (do we zero it?)
    
    char filename[fn_size];
    
    void synchronise_nodes();
    void save_temperature();
};
}
#endif
#endif
 






