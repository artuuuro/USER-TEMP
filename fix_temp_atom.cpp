/**
 * temp/grid
 * 
 * LAMMPS fix to calculate system temperature on a grid and store it in
 * a file. Also some averaging is done if needed.
 * 
 * Created by Artur Tamm <code@arturtamm.ee>
 * License GPL v3
 **/

#include <cstdio>
#include <cstdlib>
#include <algorithm>

#include "fix.h"
#include "error.h"
#include "atom.h"
#include "domain.h"
#include "force.h"
#include "update.h"

using namespace LAMMPS_NS;
using namespace FixConst;

#include "fix_temp_atom.h"

/**
   * FixEPH arguments
   * arg[ 0] <- fix ID
   * arg[ 1] <- group
   * arg[ 2] <- name
   * arg[ 3] <- Nevery
   * arg[ 4] <- Nrepeat
   * arg[ 5] <- Nfreq
   * arg[ 6] <- Rho.data
   * arg[ 7] <- type1
   * arg[ 8] <- type2
   * arg[ 9] <- ...
   **/
FixTempAtom::FixTempAtom(class LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg),
  sum_temperature {0.0},
  sum_temperature_x {0.0},
  sum_temperature_y {0.0},
  sum_temperature_z {0.0} {
  
  if(narg < 11) 
    error->all(FLERR, "fix_temp_grid: too few arguments");
  if(narg < atom->natoms < 1) 
    error->all(FLERR, "fix_temp_grid: no atoms");
  
  MPI_Comm_rank(world, &my_id);
  MPI_Comm_size(world, &nr_ps);
  
  scalar_flag = 1;
  vector_flag = 1;
  size_vector = 3;
  
  // how often do we use values
  nevery = n_every = atoi(arg[3]);
  if(n_every < 1) 
    error->all(FLERR, "fix_temp_grid: illegal n_every supplied");
  
  // how many values to use when averaging
  global_freq = n_freq = atoi(arg[4]);
  if(n_freq < 1) 
    error->all(FLERR, "fix_temp_grid: illegal n_freq supplied");
  
  // how often should we write an output
  n_out = atoi(arg[5]);
  if(n_out < n_every * n_freq) 
    error->all(FLERR, "fix_temp_grid: illegal n_out; cannot print so often");
  if(n_out % (n_every * n_freq) != 0) 
    error->all(FLERR, "fix_temp_grid: n_out not a multiple of n_freq and n_every");
  
  step = 0; // set internal step to zero
}

void FixTempAtom::init() {
  if (domain->dimension == 2)
    error->all(FLERR,"Cannot use fix eph with 2d simulation");
  if (domain->nonperiodic != 0)
    error->all(FLERR,"Cannot use nonperiodic boundares with fix eph");
  if (domain->triclinic)
    error->all(FLERR,"Cannot use fix eph with triclinic box");
}

void FixTempAtom::end_of_step() {
 
}

double FixTempAtom::compute_scalar() {
  return sum_temperature;
}

double FixTempAtom::compute_vector(int i) {
  /** DEBUG **/
  //printf("%d\n", i);
  /** DEBUG **/
  
  switch(i) {
    case 0:
      return sum_temperature_x;
    case 1:
      return sum_temperature_y;
    case 2:
      return sum_temperature_z;
    default:
      throw;
  }
  
  return 0.0;
}

double FixTempAtom::memory_usage() {
    double bytes = 0;
    
    return bytes;
}

