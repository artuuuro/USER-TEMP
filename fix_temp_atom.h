/**
 * temp/grid
 * 
 * LAMMPS fix to calculate system temperature on a grid and store it in
 * a file. Also some averaging is done if needed.
 * 
 * Created by Artur Tamm <code@arturtamm.ee>
 * License GPL v3
 **/
 
#ifdef FIX_CLASS

FixStyle(temp/atom, FixTempAtom);

#else

#ifndef LMP_FIX_TEMP_ATOM_H
#define LMP_FIX_TEMP_ATOM_H

#include "fix.h"

#include <vector>

#include "temp_atom_spline.h"

namespace LAMMPS_NS {

class FixTempAtom : public Fix {
  public:
    constexpr static int fn_size = 256;
    FixTempAtom(class LAMMPS *, int, char **);
    
    int setmask() {
      int mask = 0;
      mask |= FixConst::END_OF_STEP;
      
      return mask;
    }
    
    void init();
    void end_of_step();
    
    double compute_scalar();
    double compute_vector(int index);
    
    double memory_usage();
    
  private:
    int my_id;
    int nr_ps;
    
    std::vector<temp_atom_spline<>> rho;
    
    double sum_temperature_x;
    double sum_temperature_y;
    double sum_temperature_z;
    double sum_temperature;
    
    int n_every; // how often to sample
    int n_freq; // how often to average
    int n_out; // how often to save output
    int step; // step counter (do we zero it?)
    
    void save_temperature();
};
}
#endif
#endif
 






