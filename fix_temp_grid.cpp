/**
 * temp/grid
 * 
 * LAMMPS fix to calculate system temperature on a grid and store it in
 * a file. Also some averaging is done if needed.
 * 
 * Created by Artur Tamm <code@arturtamm.ee>
 * License GPL v3
 **/

#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <cstring>

#include "fix.h"
#include "error.h"
#include "atom.h"
#include "domain.h"
#include "force.h"
#include "update.h"

using namespace LAMMPS_NS;
using namespace FixConst;

#include "fix_temp_grid.h"

/**
   * FixTempGrid arguments
   * arg[ 0] <- fix ID
   * arg[ 1] <- group
   * arg[ 2] <- name
   * arg[ 3] <- Nevery
   * arg[ 4] <- Nrepeat
   * arg[ 5] <- Nfreq
   * arg[ 6] <- T_out / NULL
   * arg[ 7] <- box / custom
   * arg[ 8] <- Nx
   * arg[ 9] <- Ny
   * arg[10] <- Nz
   * arg[11] <- xlo
   * arg[12] <- xhi
   * arg[13] <- ylo
   * arg[14] <- yhi
   * arg[15] <- zlo
   * arg[16] <- zhi
   **/
FixTempGrid::FixTempGrid(class LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg),
  sum_temperature {0.0},
  sum_temperature_x {0.0},
  sum_temperature_y {0.0},
  sum_temperature_z {0.0} {
  
  /** DEBUG **/
  //printf("FixTempGrid: constructor\n");
  /** DEBUG **/
  
  if(narg < 11) 
    error->all(FLERR, "fix_temp_grid: too few arguments");
  if(narg < atom->natoms < 1) 
    error->all(FLERR, "fix_temp_grid: no atoms");
  
  MPI_Comm_rank(world, &my_id);
  MPI_Comm_size(world, &nr_ps);
  
  scalar_flag = 1;
  vector_flag = 1;
  size_vector = 3;
  
  // how often do we use values
  nevery = n_every = atoi(arg[3]);
  if(n_every < 1) 
    error->all(FLERR, "fix_temp_grid: illegal n_every supplied");
  
  // how many values to use when averaging
  global_freq = n_freq = atoi(arg[4]);
  if(n_freq < 1) 
    error->all(FLERR, "fix_temp_grid: illegal n_freq supplied");
  
  // how often should we write an output
  n_out = atoi(arg[5]);
  if(n_out < n_every * n_freq) 
    error->all(FLERR, "fix_temp_grid: illegal n_out; cannot print so often");
  if(n_out % (n_every * n_freq) != 0) 
    error->all(FLERR, "fix_temp_grid: n_out not a multiple of n_freq and n_every");
  
  n_step = 0; // set internal step to zero
  sprintf(filename, "%s", arg[6]);
  
  n_x = atoi(arg[8]);
  n_y = atoi(arg[9]);
  n_z = atoi(arg[10]);
  if(n_x < 1 || n_y < 1 || n_z < 1) 
    error->all(FLERR, "fix_temp_grid: invalid grid size");
  
  if(arg[7][0] == 'b') {
    x_lo = domain->boxlo[0];
    y_lo = domain->boxlo[1];
    z_lo = domain->boxlo[2];
    x_hi = domain->boxhi[0];
    y_hi = domain->boxhi[1];
    z_hi = domain->boxhi[2];
  }
  else if(arg[7][0] == 'c') {
    x_lo = atof(arg[11]);
    x_hi = atof(arg[12]);
    y_lo = atof(arg[13]);
    y_hi = atof(arg[14]);
    z_lo = atof(arg[15]);
    z_hi = atof(arg[16]);
  }
  else error->all(FLERR, "fix_temp_grid: unknown argument supplied; box or custom expected");
  
  if(x_lo >= x_hi || y_lo >= y_hi || z_lo >= z_hi)
    error->all(FLERR, "fix_temp_grid: box values invalid");
    
  dx = (x_hi-x_lo) / n_x;
  dy = (y_hi-y_lo) / n_y;
  dz = (z_hi-z_lo) / n_z;
  
  int ntotal = n_x*n_y*n_z;
  /** DEBUG **/
  //printf("%d\n", ntotal);
  /** DEBUG **/
  
  n_average.resize(ntotal, 0);
  m_average.resize(ntotal, 0.0);
  v_x_average.resize(ntotal, 0.0);
  v_y_average.resize(ntotal, 0.0);
  v_z_average.resize(ntotal, 0.0);
  temperature_x.resize(ntotal, 0.0);
  temperature_y.resize(ntotal, 0.0);
  temperature_z.resize(ntotal, 0.0);
  
  end_of_step();
}

void FixTempGrid::init() {
  /** DEBUG **/
  //printf("FixTempGrid: init\n");
  /** DEBUG **/
  
  if (domain->dimension == 2)
    error->all(FLERR,"Cannot use fix eph with 2d simulation");
  if (domain->nonperiodic != 0)
    error->all(FLERR,"Cannot use nonperiodic boundares with fix eph");
  if (domain->triclinic)
    error->all(FLERR,"Cannot use fix eph with triclinic box");
}

void FixTempGrid::end_of_step() {
  /** DEBUG **/
  //printf("FixTempGrid: end_of_step\n");
  /** DEBUG **/
  
  double **x = atom->x;
  double **v = atom->v;
  double *mass = atom->mass;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  int *type = atom->type;
  
  int ntotal = n_x * n_y * n_z;
  
  // find v_cm
  std::fill(n_average.begin(), n_average.end(), 0); // clean old values
  std::fill(m_average.begin(), m_average.end(), 0.0); // clean old values
  std::fill(v_x_average.begin(), v_x_average.end(), 0.0); // clean old values
  std::fill(v_y_average.begin(), v_y_average.end(), 0.0); // clean old values
  std::fill(v_z_average.begin(), v_z_average.end(), 0.0); // clean old values
  
  if(my_id != 0) {
    std::fill(temperature_x.begin(), temperature_x.end(), 0.0); // clean old values
    std::fill(temperature_y.begin(), temperature_y.end(), 0.0); // clean old values
    std::fill(temperature_z.begin(), temperature_z.end(), 0.0); // clean old values
  }
  
  /** DEBUG **/
  //printf("%f %f \n%f %f \n%f %f\n", x_lo, x_hi, y_lo, y_hi, z_lo, z_hi);
  //printf("%f %f %f\n", dx, dy, dz);
  /** DEBUG **/
  
  for(int i = 0; i < nlocal; ++i) {
    if(mask[i] & groupbit) {
      int index_x = std::floor((x[i][0]-x_lo) / dx);
      int px = std::floor( ((double) index_x) / n_x);
      index_x -= px*n_x;

      int index_y = std::floor((x[i][1]-y_lo) / dy);
      int py = std::floor( ((double) index_y) / n_y);
      index_y -= py * n_y;

      int index_z = std::floor((x[i][2]-z_lo) / dz);
      int pz = std::floor( ((double) index_z) / n_z);
      index_z -= pz * n_z;
      
      int index = index_x + index_y * n_x +  index_z * n_x * n_y;
      
      /** DEBUG **/
      //printf("%f %f %f %d %d %d %d\n", x[i][0], x[i][1], x[i][2], index_x, index_y, index_z, index);
      /** DEBUG **/
      
      v_x_average[index] += mass[type[i]] * v[i][0];
      v_y_average[index] += mass[type[i]] * v[i][1];
      v_z_average[index] += mass[type[i]] * v[i][2];
      m_average[index] += mass[type[i]];
      n_average[index]++;
      
      /** DEBUG **/
      //printf("%f %f %f %f %f %f\n", v[i][0], v[i][1], v[i][2], 
      //  v_x_average[index], v_y_average[index], v_z_average[index]);
      /** DEBUG **/
    }
  }
  
  // synchronise
  MPI_Allreduce(MPI_IN_PLACE, n_average.data(), ntotal, MPI_INT, MPI_SUM, world);
  MPI_Allreduce(MPI_IN_PLACE, m_average.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);

  MPI_Allreduce(MPI_IN_PLACE, v_x_average.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);
  MPI_Allreduce(MPI_IN_PLACE, v_y_average.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);
  MPI_Allreduce(MPI_IN_PLACE, v_z_average.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);
  
  /** DEBUG **/
  //printf("kB: %f  mvv2e: %f\n", force->boltz, force->mvv2e);
  /** DEBUG **/
  
  
  for(int i = 0; i < ntotal; ++i) {
    if(m_average[i] > 0.0) {
      v_x_average[i] /= m_average[i];
      v_y_average[i] /= m_average[i];
      v_z_average[i] /= m_average[i];
    }
  }
  
  /** DEBUG **/
  //for(int i = 0; i < ntotal; ++i) {
  //  printf("%d %d %f %f %f %f\n", i, n_average[i], m_average[i], 
  //    v_x_average[i], v_y_average[i], v_z_average[i]);
  //}
  /** DEBUG **/

  
  // every task has the correct center of mass now
  for(int i = 0; i < nlocal; i++) {
    if(mask[i] & groupbit) {
      int index_x = floor((x[i][0] - x_lo) / dx);
      if(index_x < 0) index_x += n_x;
      else if(index_x >= n_x) index_x -= n_x;
      
      int index_y = floor((x[i][1] - y_lo) / dy);
      if(index_y < 0) index_y += n_y;
      else if(index_y >= n_y) index_y -= n_y;
      
      int index_z = floor((x[i][2] - z_lo) / dz);
      if(index_z < 0) index_z += n_z;
      else if(index_z >= n_z) index_z -= n_z;
      
      int index = index_x + index_y * n_x +  index_z * n_x * n_y;
      
      if(n_average[index] > 1) {
        double vx2 = (v[i][0] - v_x_average[index])*(v[i][0] - v_x_average[index]);
        double vy2 = (v[i][1] - v_y_average[index])*(v[i][1] - v_y_average[index]);
        double vz2 = (v[i][2] - v_z_average[index])*(v[i][2] - v_z_average[index]);
      
        double prefactor = mass[type[i]] / ((n_average[index]-1) * n_freq) / force->boltz * force->mvv2e;
      
        temperature_x[index] += prefactor * vx2;
        temperature_y[index] += prefactor * vy2;
        temperature_z[index] += prefactor * vz2;
        
        /** DEBUG **/
        //printf("%d %d %d %f %f %f %f %f\n", index, n_average[index], n_freq, 
        //  mass[type[i]], prefactor, vx2, vy2, vz2);
        /** DEBUG **/
      }
    }
  }
  
  // synchronise temperatures
  MPI_Allreduce(MPI_IN_PLACE, temperature_x.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);
  MPI_Allreduce(MPI_IN_PLACE, temperature_y.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);
  MPI_Allreduce(MPI_IN_PLACE, temperature_z.data(), ntotal, MPI_DOUBLE, MPI_SUM, world);
  
  /** DEBUG **/
  //for(int i = 0; i < ntotal; ++i) {
  //  printf("%d %f %f %f %f\n", i, temperature_x[i], temperature_y[i], temperature_z[i], sum_temperature);
  //}
  /** DEBUG **/
  
  if((n_step % n_freq) == 0) {
    // special action if step = 0;
    if(n_step == 0) {
      for(int i = 0; i < ntotal; ++i) {
        temperature_x[i] *= n_freq;
        temperature_y[i] *= n_freq;
        temperature_z[i] *= n_freq;
      }
    }
    
    // maybe use accumulate here
    sum_temperature_x = 0.0;
    sum_temperature_y = 0.0;
    sum_temperature_z = 0.0;
    
    for(int i = 0; i < ntotal; ++i) {
      sum_temperature_x += temperature_x[i];
      sum_temperature_y += temperature_y[i];
      sum_temperature_z += temperature_z[i];
    }
    
    sum_temperature_x /= ntotal;
    sum_temperature_y /= ntotal;
    sum_temperature_z /= ntotal;
    
    sum_temperature = (sum_temperature_x + sum_temperature_y + sum_temperature_z) / 3.0;
    
    /** DEBUG **/
    //printf("%d %d %f %f %f %f\n", step, step / n_out, 
    //  sum_temperature_x, sum_temperature_y, sum_temperature_z, sum_temperature);
    /** DEBUG **/
    
    if(((update->ntimestep % n_out) == 0) && (my_id == 0)) save_temperature();
    
    std::fill(temperature_x.begin(), temperature_x.end(), 0.0);
    std::fill(temperature_y.begin(), temperature_y.end(), 0.0);
    std::fill(temperature_z.begin(), temperature_z.end(), 0.0);
  }
  
  /** DEBUG **/
  //printf("%d %ld\n", step, update->ntimestep);
  /** DEBUG **/
  n_step++;
}

double FixTempGrid::compute_scalar() {
  /** DEBUG **/
  //printf("FixTempGrid: compute_scalar\n");
  /** DEBUG **/
  return sum_temperature;
}

double FixTempGrid::compute_vector(int i) {
  /** DEBUG **/
  //printf("FixTempGrid: compute_vector\n");
  /** DEBUG **/
  
  /** DEBUG **/
  //printf("%d\n", i);
  /** DEBUG **/
  
  switch(i) {
    case 0:
      return sum_temperature_x;
    case 1:
      return sum_temperature_y;
    case 2:
      return sum_temperature_z;
    default:
      throw;
  }
  
  return 0.0;
}

// this is not used right now
void FixTempGrid::synchronise_nodes() {

}

void FixTempGrid::save_temperature() {
  /** DEBUG **/
  //printf("FixTempGrid: save_temperature\n");
  /** DEBUG **/
  
  if(! std::strcmp(filename, "NULL")) return;
  
  char fn[fn_size];
  /** DEBUG **/
  //printf("%d %lld %d %lld\n", my_id, update->ntimestep, n_out, update->ntimestep/n_out);
  /** DEBUG **/
  
  unsigned int n = (update->ntimestep)/n_out;
  sprintf(fn, "%s_%06d", filename, n);
  FILE *fd = fopen(fn, "w");
  
  if(!fd) throw; // give an error?
  
  // this is needed for visit Point3D
  fprintf(fd, "x y z T_x T_y T_z\n");
  
  for(int k = 0; k < n_z; ++k) {
    for(int j = 0; j < n_y; ++j) {
      for(int i = 0; i < n_x; ++i) {
        unsigned int index = i + j * n_x + k * n_x * n_y;
        
        double x = x_lo + i * dx;
        double y = y_lo + j * dy;
        double z = z_lo + k * dz;
        
        fprintf(fd, "%.6e %.6e %.6e %.6e %.6e %.6e\n", x, y, z, 
          temperature_x[index], temperature_y[index], temperature_z[index]);
      }
    }
  }
  
  fclose(fd);
  
  
}

double FixTempGrid::memory_usage() {
  /** DEBUG **/
  //printf("FixTempGrid: memory_usage\n");
  /** DEBUG **/
  
  double bytes = 0.0;
  
  return bytes;
}

